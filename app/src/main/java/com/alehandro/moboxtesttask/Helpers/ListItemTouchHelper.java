package com.alehandro.moboxtesttask.Helpers;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.alehandro.moboxtesttask.Adapters.PhotoListItemAdapter;

public class ListItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private PhotoListItemAdapter mPhotoListItemAdapter;

    public ListItemTouchHelper(PhotoListItemAdapter adapter) {
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        mPhotoListItemAdapter = adapter;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mPhotoListItemAdapter.remove(viewHolder.getAdapterPosition());
    }
}
