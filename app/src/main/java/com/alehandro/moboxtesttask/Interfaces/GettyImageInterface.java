package com.alehandro.moboxtesttask.Interfaces;

import com.alehandro.moboxtesttask.Constants.Constants;
import com.alehandro.moboxtesttask.POJO.GetDetailImageResponse;
import com.alehandro.moboxtesttask.POJO.GettyImageResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GettyImageInterface {
//    @Headers(Constants.API_KEY)
//    @GET("v3/search/images")
//    Observable<GettyImageResponse> getPhotos(@Query("fields") String fields,
//                                             @Query("file_types") String imageType,
//                                             @Query("graphical_styles") String graphicalStyles,
//                                             @Query("orientations") String orientations,
//                                             @Query("page_size") int page_size);

    @Headers(Constants.API_KEY)
    @GET("/v3/search/images?fields=thumb%2Ctitle&file_types=jpg&graphical_styles=illustration")
    Observable<GettyImageResponse> getPhotos();

    @Headers(Constants.API_KEY)
    @GET("/v3/images/{id}?fields=display_set")
    Observable<GetDetailImageResponse> getSelectedImage(@Path("id") String id);

}
