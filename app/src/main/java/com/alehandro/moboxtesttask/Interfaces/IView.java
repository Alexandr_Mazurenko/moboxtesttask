package com.alehandro.moboxtesttask.Interfaces;

public interface IView {
    void initUI();

    void updateUI();
}
