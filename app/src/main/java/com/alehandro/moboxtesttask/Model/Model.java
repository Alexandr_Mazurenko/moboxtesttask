package com.alehandro.moboxtesttask.Model;

import com.alehandro.moboxtesttask.POJO.PhotoItem;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private static Model instance;
    private List<PhotoItem> mPhotoItemList;
    private String mChosenTitle;
    private String mChosenImageUrl;
    private String mChosenId;

    private Model() {
        mPhotoItemList = new ArrayList<>();
    }

    public static Model getInstance() {
        if (instance == null) {
            instance = new Model();
        }
        return instance;
    }

    public void setChosenId(int position) {
        mChosenId = mPhotoItemList.get(position).getId();
    }

    public String getChosenId() {
        return mChosenId;
    }

    public String getChosenTitle() {
        return mChosenTitle;
    }

    public void setChosenTitle(int position) {
        mChosenTitle = mPhotoItemList.get(position).getTitle();
    }

    public String getChosenImageUrl() {
        return mChosenImageUrl;
    }

    public void setChosenImageUrl(String mChosenImageUrl) {
        this.mChosenImageUrl = mChosenImageUrl;
    }

    public List<PhotoItem> getPhotoItemList() {
        return mPhotoItemList;
    }

    public void setPhotoItemList(List<PhotoItem> mPhotoItemList) {
        this.mPhotoItemList = mPhotoItemList;
    }
}
