package com.alehandro.moboxtesttask.Interactors;

import android.util.Log;

import com.alehandro.moboxtesttask.Constants.Constants;
import com.alehandro.moboxtesttask.Helpers.RetrofitHelper;
import com.alehandro.moboxtesttask.Interfaces.IView;
import com.alehandro.moboxtesttask.Model.Model;
import com.alehandro.moboxtesttask.POJO.GetDetailImageResponse;
import com.alehandro.moboxtesttask.POJO.GettyImageResponse;
import com.alehandro.moboxtesttask.POJO.Image;
import com.alehandro.moboxtesttask.POJO.PhotoItem;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WebInteractor {
    private IView mIView;
    private Subscription mSubscription;
    private List<PhotoItem> mBufferList;
    private String mSelectedImageUrl;
    private Model mModel;

    public WebInteractor() {
        mModel = Model.getInstance();
    }

    public void initialize(IView iView) {
        mIView = iView;
    }

    public boolean isPhotoListEmpty() {
        return mModel.getPhotoItemList().isEmpty();
    }

    //get data for list of items
    public void fetchImageSearchData() {
//        Observable<GettyImageResponse> responseList =
//                RetrofitHelper.getInstance().
//                        getGettyImageInterface().getPhotos(Constants.Queries.THUMB_AND_TITLE,
//                        Constants.Queries.IMAGE_TYPE_JPG,
//                        Constants.Queries.STYLE_FINE_ART,
//                        Constants.Queries.ORIENTATION_SQUARE,
//                        Constants.Queries.PER_PAGE_TEN);
        mBufferList = new ArrayList<>();
        Observable<GettyImageResponse> responseList =
                RetrofitHelper.getInstance()
                        .getGettyImageInterface()
                        .getPhotos();
        responseList.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GettyImageResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.i(Constants.LOG_TAG, "onSubscribe");
                    }

                    @Override
                    public void onNext(GettyImageResponse gettyImageResponse) {
                        for (Image image : gettyImageResponse.getImages()) {
                            PhotoItem photoItem = new PhotoItem();
                            photoItem.setTitle(image.getTitle());
                            photoItem.setUrl(image.getDisplaySizes().get(0)
                                    .getUri());
                            photoItem.setId(image.getId());
                            mBufferList.add(photoItem);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(Constants.LOG_TAG, e.getLocalizedMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.i(Constants.LOG_TAG, "Completed!");
                        mModel.setPhotoItemList(mBufferList);
                        mIView.updateUI();
                    }
                });
    }

    //get a bigger image of selected photo
    public void getSelectedImage() {
        Observable<GetDetailImageResponse> detailImageResponse = RetrofitHelper.getInstance()
                .getGettyImageInterface()
                .getSelectedImage(mModel.getChosenId());
        Log.i(Constants.LOG_TAG, "chosenId=" + mModel.getChosenId());
        detailImageResponse.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetDetailImageResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(GetDetailImageResponse getDetailImageResponse) {
                        mSelectedImageUrl = getDetailImageResponse.getImages().get(0)
                                .getDisplaySizes()
                                .get(0)
                                .getUri();

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(Constants.LOG_TAG, "get_selected_image:" + e.getLocalizedMessage());
                    }

                    @Override
                    public void onComplete() {
                        mModel.setChosenImageUrl(mSelectedImageUrl);
                        mIView.updateUI();
                    }
                });

    }

    public boolean isChosenEmpty() {
        return mModel.getChosenImageUrl().isEmpty();
    }
}
