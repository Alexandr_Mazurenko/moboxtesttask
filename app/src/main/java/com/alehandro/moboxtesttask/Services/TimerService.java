package com.alehandro.moboxtesttask.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.provider.SyncStateContract;
import android.util.Log;
import android.widget.Toast;

import com.alehandro.moboxtesttask.Constants.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class TimerService extends Service {
    private Timer mTimer = null;

    public TimerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimeMessageTask(), TimeUnit.MINUTES
                .toMillis(Constants.TIME_MESSAGE_DELAY), TimeUnit.MINUTES
                .toMillis(Constants.TIME_MESSAGE_DELAY));
    }

    private class TimeMessageTask extends TimerTask {
        @Override
        public void run() {
            Intent intent = new Intent(Constants.BROADCAST_INTENT_TAG);
            SimpleDateFormat dateFormat = new SimpleDateFormat("[hh:mm]");
            String timeMessage = dateFormat.format(new Date());
            intent.putExtra(Constants.BROADCAST_TIME_MESSAGE, timeMessage);
            sendBroadcast(intent);
        }
    }
}
