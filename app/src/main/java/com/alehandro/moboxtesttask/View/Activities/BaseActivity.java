package com.alehandro.moboxtesttask.View.Activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.alehandro.moboxtesttask.Constants.Constants;
import com.alehandro.moboxtesttask.R;
import com.squareup.picasso.Picasso;

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog mProgressdialog;
    private AlertDialog mAlertDialog;
    private View mAlertView;
    private TimeMessageBroadcastReceiver receiver = new TimeMessageBroadcastReceiver();

    void showProgressDialog() {
        mProgressdialog = new ProgressDialog(BaseActivity.this);
        mProgressdialog.setTitle("Please, wait...");
        mProgressdialog.show();
    }

    void hideProgressDialog() {
        mProgressdialog.hide();
    }

    void showCustomDialog(String timeMessage) {
        mAlertView = View.inflate(this, R.layout.time_message_dialog, null);
        ImageView alertImage = (ImageView) mAlertView.findViewById(R.id.dialogImage);
        Picasso.with(this).load(Constants.ALERT_DIALOG_IMAGE_URL).into(alertImage);
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setView(mAlertView)
                .setMessage(timeMessage)
                .setPositiveButton("Ok", (dialog, which) -> mAlertDialog.dismiss());
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }

    void registerTimeMessageBroadcastReceiver() {
        registerReceiver(receiver, new IntentFilter(Constants.BROADCAST_INTENT_TAG));
    }

    void unregisterTimeMessageBroadcastReceiver() {
        unregisterReceiver(receiver);
    }

    class TimeMessageBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            showCustomDialog(intent.getStringExtra(Constants.BROADCAST_TIME_MESSAGE));
        }
    }


}
