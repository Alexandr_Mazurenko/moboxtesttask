package com.alehandro.moboxtesttask.View.Activities;

import android.databinding.DataBindingUtil;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.alehandro.moboxtesttask.Constants.Constants;
import com.alehandro.moboxtesttask.Interactors.WebInteractor;
import com.alehandro.moboxtesttask.Interfaces.IView;
import com.alehandro.moboxtesttask.R;
import com.alehandro.moboxtesttask.ViewModel.DetailViewModel;
import com.alehandro.moboxtesttask.databinding.ActivitySecondBinding;
import com.squareup.picasso.Picasso;

public class SecondActivity extends BaseActivity implements IView {
    private WebInteractor mWebInteractor;
    private ActivitySecondBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        registerTimeMessageBroadcastReceiver();
        mWebInteractor = new WebInteractor();
        if (mWebInteractor.isChosenEmpty()) {
            mWebInteractor.initialize(this);
            mWebInteractor.getSelectedImage();
            showProgressDialog();
        } else {
            mBinding.setDvm(new DetailViewModel());
        }
    }

    @Override
    public void initUI() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_second);
    }

    @Override
    public void updateUI() {
        hideProgressDialog();
        mBinding.setDvm(new DetailViewModel());
    }

    @Override
    protected void onDestroy() {
        unregisterTimeMessageBroadcastReceiver();
        mWebInteractor = null;
        super.onDestroy();
    }

}
