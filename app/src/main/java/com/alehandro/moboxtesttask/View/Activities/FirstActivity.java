package com.alehandro.moboxtesttask.View.Activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.alehandro.moboxtesttask.Adapters.PhotoListItemAdapter;
import com.alehandro.moboxtesttask.Helpers.ListItemTouchHelper;
import com.alehandro.moboxtesttask.Interactors.WebInteractor;
import com.alehandro.moboxtesttask.Interfaces.IView;
import com.alehandro.moboxtesttask.R;
import com.alehandro.moboxtesttask.databinding.ActivityFirstBinding;

public class FirstActivity extends BaseActivity implements IView {
    private WebInteractor mWebInteractor;
    private ActivityFirstBinding binding;
    private PhotoListItemAdapter mPhotoListItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        registerTimeMessageBroadcastReceiver();
        mWebInteractor = new WebInteractor();
        if (mWebInteractor.isPhotoListEmpty()) {
            mWebInteractor.initialize(this);
            mWebInteractor.fetchImageSearchData();
            showProgressDialog();
        }
    }


    @Override
    public void initUI() {
        binding = DataBindingUtil.setContentView(this,
                R.layout.activity_first);
        binding.photoList.setLayoutManager(new LinearLayoutManager(this));
        mPhotoListItemAdapter = new PhotoListItemAdapter();
        binding.photoList.setAdapter(mPhotoListItemAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(new ListItemTouchHelper
                (mPhotoListItemAdapter));
        helper.attachToRecyclerView(binding.photoList);
    }

    @Override
    public void updateUI() {
        mPhotoListItemAdapter.notifyDataSetChanged();
        hideProgressDialog();
    }

    @Override
    protected void onDestroy() {
        mWebInteractor = null;
        unregisterTimeMessageBroadcastReceiver();
        super.onDestroy();
    }
}

