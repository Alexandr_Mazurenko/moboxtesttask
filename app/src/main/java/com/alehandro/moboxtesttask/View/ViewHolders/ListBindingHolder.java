package com.alehandro.moboxtesttask.View.ViewHolders;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.alehandro.moboxtesttask.databinding.ListItemBinding;

public class ListBindingHolder extends RecyclerView.ViewHolder {
    public ListItemBinding getBinding() {
        return mBinding;
    }

    private ListItemBinding mBinding;

    public ListBindingHolder(ListItemBinding binding) {
        super(binding.photoCard);
        mBinding = binding;
        mBinding.executePendingBindings();
    }
}
