package com.alehandro.moboxtesttask.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class GetDetailImageResponse {
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("images_not_found")
    @Expose
    private List<Object> imagesNotFound = null;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<Object> getImagesNotFound() {
        return imagesNotFound;
    }

    public void setImagesNotFound(List<Object> imagesNotFound) {
        this.imagesNotFound = imagesNotFound;
    }
}
