package com.alehandro.moboxtesttask;

import android.app.Application;
import android.content.Intent;

import com.alehandro.moboxtesttask.Services.TimerService;

public class TestTaskApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        startService(new Intent(this, TimerService.class));
    }

    @Override
    public void onTerminate() {
        stopService(new Intent(this, TimerService.class));
        super.onTerminate();
    }
}
