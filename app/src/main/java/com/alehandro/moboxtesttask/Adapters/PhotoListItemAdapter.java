package com.alehandro.moboxtesttask.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alehandro.moboxtesttask.Model.Model;
import com.alehandro.moboxtesttask.R;
import com.alehandro.moboxtesttask.View.Activities.SecondActivity;
import com.alehandro.moboxtesttask.View.ViewHolders.ListBindingHolder;
import com.alehandro.moboxtesttask.ViewModel.PhotoViewModel;
import com.alehandro.moboxtesttask.databinding.ListItemBinding;

public class PhotoListItemAdapter extends RecyclerView.Adapter<ListBindingHolder> {
    private Context mContext;

    public PhotoListItemAdapter() {
        super();
    }

    @Override
    public ListBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        ListItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.list_item, parent, false);
        return new ListBindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(ListBindingHolder holder, int position) {
        ListItemBinding binding = holder.getBinding();
        binding.setPvm(new PhotoViewModel(Model.getInstance().getPhotoItemList().get(position)));
        binding.photoCard.setOnClickListener(v -> {
            Model.getInstance().setChosenId(position);
            Model.getInstance().setChosenTitle(position);
            Model.getInstance().setChosenImageUrl("");

            Intent intent = new Intent(mContext, SecondActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mContext
                        , binding.itemPhoto, "gettyImage");
                mContext.startActivity(intent, optionsCompat.toBundle());
            } else
                mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return Model.getInstance().getPhotoItemList().size();
    }

    public void remove(int position) {
        Model.getInstance().getPhotoItemList().remove(position);
        notifyItemRemoved(position);
    }
}
