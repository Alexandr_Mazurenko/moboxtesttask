package com.alehandro.moboxtesttask.Constants;

public class Constants {
    public final static String SERVER_URL = "https://api.gettyimages.com:443";
    public final static String API_KEY = "Api-Key: 8x8p87xd8vskmccsv6rasrga";
    public final static String LOG_TAG = "MOBOXTestTask";
    public final static String SHARED_TRANSITION_VIEW_NAME = "SHARED_NAME";
    public final static String BROADCAST_INTENT_TAG = "TestTaskIntent";
    public final static String BROADCAST_TIME_MESSAGE = "TestTaskIntentTime";
    public final static int TIME_MESSAGE_DELAY = 4;
    public final static String ALERT_DIALOG_IMAGE_URL = "https://lh3.googleusercontent.com/" +
            "7yuNfD5OL0SKm41JZqpXfHLpkIhFG0EzPXLbIr_pL7pYdPaH8trvfH3TCKKL6d4d630=w300-rw";

    public static class Queries {
        public final static String THUMB_AND_TITLE = "thumb%2Ctitle";
        public final static String IMAGE_TYPE_JPG = "jpg";
        public final static String STYLE_FINE_ART = "fine_art";
        public final static String ORIENTATION_SQUARE = "Square";
        public final static int PER_PAGE_TEN = 10;
    }
}
