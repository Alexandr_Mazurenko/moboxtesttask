package com.alehandro.moboxtesttask.ViewModel;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.VideoView;

import com.alehandro.moboxtesttask.POJO.PhotoItem;
import com.alehandro.moboxtesttask.R;
import com.squareup.picasso.Picasso;

public class PhotoViewModel extends BaseObservable {
    private PhotoItem mPhotoItem;

    public PhotoViewModel(PhotoItem photoItem){
        mPhotoItem = photoItem;
       }

    @Bindable
    public String getTitle(){
        return mPhotoItem.getTitle();
    }

    public void setTitle(String title){
        mPhotoItem.setTitle(title);
        notifyPropertyChanged(R.id.item_title);
    }

    public String getImageUrl(){
        return mPhotoItem.getUrl();
    }

    @BindingAdapter({"item_photo"})
    public static void loadImage(ImageView imageView, String url){
        Picasso.with(imageView.getContext()).load(url).into(imageView);
    }
}
