package com.alehandro.moboxtesttask.ViewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.alehandro.moboxtesttask.Model.Model;
import com.squareup.picasso.Picasso;


public class DetailViewModel extends BaseObservable{
    private Model mModel;

    public DetailViewModel(){
        mModel = Model.getInstance();
    }

    @Bindable
    public String getTitle(){
        return mModel.getChosenTitle();
    }

    public String getImageUrl(){return mModel.getChosenImageUrl();}

    @BindingAdapter({"detail_item_photo"})
    public static void loadDetailImage(ImageView imageView, String url){
        Picasso.with(imageView.getContext()).load(url).into(imageView);
    }




}
